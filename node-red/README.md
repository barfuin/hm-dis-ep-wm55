# HM-Dis-EP-WM55 Node-RED Subflow

Here's a little [Node-RED](https://nodered.org/) subflow which can be imported using the
[Import](https://nodered.org/docs/user-guide/editor/workspace/import-export) menu item in Node-RED.

![Subflow](README-1.png)

After importing, the subflow appears in the node list under the *ccu* category. It is called *HM Dis EP WM55*.

The flow is self-documenting. Please read the flow and node information for details on how to use and modify it.

![Configuration](README-2.png)


### Prerequisites

This subflow builds on [node-red-contrib-ccu](https://flows.nodered.org/node/node-red-contrib-ccu).

And of course it uses our own [serializer](../serializer) and [config string spec](../spec/).


## Features

- Prevents the e-ink display from being updated too often. By default, an interval of 1 minute is set, but
  this can be changed in the subflow. Within this interval, all messages are aggregated, and then the
  correct and current information is displayed.
- Multiple instances of this subflow can be defined and configured with different information to display.
  The rate limiting works across all of them.
- Multiple HM-Dis-EP-WM55 displays supported.
- In order to conserve battery power, the display is only updated when contents has actually changed.
- Especially in contrast to node-red-contric-ccu's *display* node, this subflow can update individual lines of the
  display while leaving others unchanged. Also, it can refer to the pre-defined texts from the CCU config. Plus the
  rate limiting, of course. We may not have developed this subflow had we known of *display* at the time, but now
  we think it is quite nice to have.


## Status

This subflow is not a product. Use it as a starting point for your own projects. It solves quite a number
of problems already, most notably the config string generation. But it's not a tried and tested piece of
software. Major updates are not planned.


## Known Issues

Due to a firmware error (we believe), the data points are incorrectly published by the device. In order to transmit
the config string to the display, we address the data point `BidCos-RF.NEQ1234567:3.SUBMIT` (with your device's serial
number). This leads to the following warning being displayed in the Node-RED debug log:

    unknown paramsetDescription  BidCos-RF/HM-Dis-EP-WM55/1.2/11/KEY/VALUES SUBMIT

As far as we know, this warning can be safely ignored.


## Contributing

Contributions are very welcome, and will be promptly reviewed.
If you think this flow can be improved, or you want to support a new feature that would be helpful to others,
please share it in the form of merge requests. If you plan on spending a lot of effort on a contribution,
let's discuss it first in a GitLab issue.


## License

The code in this directory is provided under the terms of the [MIT license](LICENSE).
