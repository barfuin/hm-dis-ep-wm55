/**
 * Icons which can be displayed on each line.
 */
exports.Icon = {
    OFF: 1,
    ON: 2,
    OPEN: 3,
    CLOSED: 4,
    ERROR: 5,
    OK: 6,
    INFO: 7,
    NEW_MSG: 8,
    SERVICE_MSG: 9,

    _meta: {
        first: 1,
        last: 9
    }
};


/**
 * Colors in which the LED can flash.
 */
exports.LedColor = {
    OFF: 1,
    RED: 2,
    GREEN: 3,
    ORANGE: 4,

    _meta: {
        first: 1,
        last: 4
    }
};


/**
 * Beep patterns for the audible signal.
 */
exports.Beeps = {
    OFF: 1,
    LONG_LONG: 2,
    LONG_SHORT: 3,
    LONG_SHORT_SHORT: 4,
    SHORT: 5,
    SHORT_SHORT: 6,
    LONG: 7,

    _meta: {
        first: 1,
        last: 7
    }
};


/** maximum number of characters per line */
const MAX_LINE_LEN = 12;


normalizeLineConfig = function(lineConfig) {
    let block = undefined;
    let text = undefined;
    let icon = undefined;

    if (lineConfig) {
        if (typeof(lineConfig) === 'string' && lineConfig.length > 0) {
            text = lineConfig;
        } else if (typeof(lineConfig.block) === 'number' && lineConfig.block >= 1 && lineConfig.block <= 10) {
            block = lineConfig.block;
        } else if (typeof(lineConfig.text) === 'string' && lineConfig.text.length > 0) {
            text = lineConfig.text;
        }
        if (text && text.length > MAX_LINE_LEN) {
            text = text.substr(0, MAX_LINE_LEN);
        }

        if (lineConfig.icon && typeof(lineConfig.icon) === 'number' &&
            lineConfig.icon >= exports.Icon._meta.first && lineConfig.icon <= exports.Icon._meta.last) {
            icon = lineConfig.icon;
        }
    }
    return block ? { block, icon } : (text ? { text, icon } : undefined);
}

/**
 * lineConfig: string
 * lineConfig: { text: string }
 * lineConfig: { text: string, icon: Icon }
 * lineConfig: { block: number }
 * lineConfig: { block: number, icon: Icon }
 * When both block and text are specified, we use block.
 */
serializeLine = function(lineConfig) {
    lineConfig = normalizeLineConfig(lineConfig);

    const specialChars = {
        'Ä': '0x5B',
        'Ö': '0x23',
        'Ü': '0x24',
        'ä': '0x7B',
        'ö': '0x7C',
        'ü': '0x7D',
        'ß': '0x5F',
        '\'': '0xB5',
        '&': '0x5D',
        '↑': '0x3D', // &uarr;
        '↗': '0x3E', // &UpperRightArrow;
        '↓': '0x3C', // &darr;
        '⌛': '0x3B' // &#8987;  (hourglass)
    };

    let result = '';
    if (lineConfig) {
        result += '0x12,';

        if (lineConfig.block) {
            result += '0x8' + (lineConfig.block - 1) + ',';
        } else {
            for (var i = 0; i < lineConfig.text.length; i++) {
                if (specialChars[lineConfig.text[i]]) {
                    result += specialChars[lineConfig.text[i]] + ',';
                } else {
                    result += '0x' + lineConfig.text.charCodeAt(i).toString(16).toUpperCase() + ',';
                }
            }
        }

        if (lineConfig.icon) {
            result += '0x13,';
            result += '0x8' + (lineConfig.icon - 1) + ',';
        }
    }
    result += '0x0A,';
    return result;
}


/**
 * {
 *   signal: Beeps.LONG_LONG,
 *   quantity: 1,
 *   distance: 30
 * }
 */
serializeBeeps = function(beepConfig) {
    let result = '';
    if (beepConfig) {
        if (beepConfig.signal && typeof(beepConfig.signal) === 'number' &&
            beepConfig.signal >= exports.Beeps._meta.first && beepConfig.signal <= exports.Beeps._meta.last) {
            result += '0xC' + (beepConfig.signal - 1) + ',';
        } else {
            result += '0xC0,';
        }

        result += '0x1C,';

        if (beepConfig.quantity && typeof(beepConfig.quantity) === 'number') {
            if (beepConfig.quantity <= 1) {
                result += '0xD0,';
            } else if (beepConfig.quantity > 15) {
                result += '0xDF,';
            } else {
                result += '0xD' + (beepConfig.quantity - 1).toString(16).toUpperCase() + ',';
            }
        } else {
            result += '0xD0,';
        }

        result += '0x1D,';

        if (beepConfig.distance && typeof(beepConfig.distance) === 'number') {
            if (beepConfig.distance <= 10) {
                result += '0xE0,';
            } else if (beepConfig.distance > 154) {
                result += '0xEF,';
            } else {
                result += '0xE' + (Math.floor(beepConfig.distance / 10) - 1).toString(16).toUpperCase() + ',';
            }
        } else {
            result += 'null,';
        }
    } else {
        result += '0xC0,0x1C,0xD0,0x1D,null,';
    }
    return result;
}


/**
 * ledFlash: LedColor
 */
serializeLedFlashes = function(ledFlash) {
    let result = 'null,';
    if (ledFlash && typeof(ledFlash) === 'number') {
        if (ledFlash <= exports.LedColor._meta.first || ledFlash > exports.LedColor._meta.last) {
            result = '0xF0,';
        } else {
            result = '0xF' + (ledFlash - 1) + ',';
        }
    }
    return result;
}


exports.buildConfigString = function(displayConfig) {
    const result = '0x02,0x0A,' +
        serializeLine(displayConfig ? displayConfig.line1 : undefined) +
        serializeLine(displayConfig ? displayConfig.line2 : undefined) +
        serializeLine(displayConfig ? displayConfig.line3 : undefined) +
        '0x0A,0x14,' +
        serializeBeeps(displayConfig ? displayConfig.sound : undefined) +
        '0x16,' +
        serializeLedFlashes(displayConfig ? displayConfig.ledFlash : undefined) +
        '0x03';
    return result;
}
