'use strict';

var expect = require('chai').expect;
var serializer = require('../src/index');
require('mocha');


describe('The buildConfigString() function', () => {

    it('handles null input', () => {
        const result = serializer.buildConfigString(null);
        expect(result).to.equal(
            '0x02,0x0A,0x0A,0x0A,0x0A,0x0A,0x14,0xC0,0x1C,0xD0,0x1D,null,0x16,null,0x03');
    });

    it('handles undefined input', () => {
        const result = serializer.buildConfigString(undefined);
        expect(result).to.equal(
            '0x02,0x0A,0x0A,0x0A,0x0A,0x0A,0x14,0xC0,0x1C,0xD0,0x1D,null,0x16,null,0x03');
    });

    it('handles empty input', () => {
        const result = serializer.buildConfigString({});
        expect(result).to.equal(
            '0x02,0x0A,0x0A,0x0A,0x0A,0x0A,0x14,0xC0,0x1C,0xD0,0x1D,null,0x16,null,0x03');
    });

    it('handles input in which all fields are null', () => {
        const result = serializer.buildConfigString({
            line1: null,
            line2: null,
            line3: null,
            sound: null,
            ledFlash: null
        });
        expect(result).to.equal(
            '0x02,0x0A,0x0A,0x0A,0x0A,0x0A,0x14,0xC0,0x1C,0xD0,0x1D,null,0x16,null,0x03');
    });

    it('handles input with text block references', () => {
        const result = serializer.buildConfigString({
            line1: { block: 2 },
            line2: { block: 3, icon: serializer.Icon.CLOSED },
            line3: { block: null },
            sound: {},
            ledFlash: 'INVALID'
        });
        expect(result).to.equal(
            '0x02,0x0A,0x12,0x81,0x0A,0x12,0x82,0x13,0x83,0x0A,0x0A,0x0A,0x14,0xC0,0x1C,0xD0,0x1D,null,0x16,null,0x03'
        );
    });

    it('handles Hello World input, silent, green LED', () => {
        const result = serializer.buildConfigString({
            line1: 'Hello',
            line2: { text: 'World', icon: null },
            line3: undefined,
            sound: undefined,
            ledFlash: serializer.LedColor.GREEN
        });
        expect(result).to.equal('0x02,0x0A,0x12,0x48,0x65,0x6C,0x6C,0x6F,0x0A,' +
            '0x12,0x57,0x6F,0x72,0x6C,0x64,0x0A,0x0A,0x0A,0x14,0xC0,0x1C,0xD0,0x1D,null,0x16,0xF2,0x03'
        );
    });

    it('handles line config so that \'block\' takes precedence over \'text\'', () => {
        const result = serializer.buildConfigString({
            line1: {
                block: 2,
                text: 'foo'
            }
        });
        expect(result).to.equal(
            '0x02,0x0A,0x12,0x81,0x0A,0x0A,0x0A,0x0A,0x14,0xC0,0x1C,0xD0,0x1D,null,0x16,null,0x03');
    });

    it('handles special characters, oversized lines, sound', () => {
        const result = serializer.buildConfigString({
            line1: '12345678901234567890', // too long
            line2: 'ÄÖÜ↑äöü↓:0°C',
            line3: {
                text: null,
                icon: serializer.Icon.NEW_MSG
            },
            sound: {
                signal: serializer.Beeps.SHORT,
                quantity: 3,
                distance: 30
            },
            ledFlash: undefined
        });
        expect(result).to.equal(
            '0x02,0x0A,0x12,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30,0x31,0x32,0x0A,' +
            '0x12,0x5B,0x23,0x24,0x3D,0x7B,0x7C,0x7D,0x3C,0x3A,0x30,0xB0,0x43,0x0A,0x0A,0x0A,' +
            '0x14,0xC4,0x1C,0xD2,0x1D,0xE2,0x16,null,0x03');
    });

    it('handles invalid numbers (too low)', () => {
        const result = serializer.buildConfigString({
            line1: { block: -1, icon: -1 },
            line2: { text: 'foo', icon: -1 },
            sound: {
                signal: serializer.Beeps.SHORT,
                quantity: -1,
                distance: -1
            },
            ledFlash: -1
        });
        expect(result).to.equal(
            '0x02,0x0A,0x0A,0x12,0x66,0x6F,0x6F,0x0A,0x0A,0x0A,0x14,0xC4,0x1C,0xD0,0x1D,0xE0,0x16,0xF0,0x03'
        );
    });

    it('handles invalid numbers (too high)', () => {
        const result = serializer.buildConfigString({
            line1: { block: 11, icon: 100 },
            line2: { text: 'foo', icon: 100 },
            sound: {
                signal: serializer.Beeps.SHORT,
                quantity: 17,
                distance: 200
            },
            ledFlash: 5
        });
        expect(result).to.equal(
            '0x02,0x0A,0x0A,0x12,0x66,0x6F,0x6F,0x0A,0x0A,0x0A,0x14,0xC4,0x1C,0xDF,0x1D,0xEF,0x16,0xF0,0x03'
        );
    });

    it('handles inconsistent sound spec', () => {
        const result = serializer.buildConfigString({
            line1: 'foo',
            sound: {
                signal: serializer.Beeps.SHORT,
                quantity: 5,
                distance: 0 // this will be interpreted by the real device as 10s
            }
        });
        expect(result).to.equal(
            '0x02,0x0A,0x12,0x66,0x6F,0x6F,0x0A,0x0A,0x0A,0x0A,0x14,0xC4,0x1C,0xD4,0x1D,null,0x16,null,0x03'
        );
    });
});
