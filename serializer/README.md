# serializer

JavaScript serializer for HM-Dis-EP-WM55 config strings.

I use it only as part of the [Node-RED subflow](../node-red), but set it up as a stand-alone JavaScript project to
have better unit testing capabilities.

Invoke via `buildConfigString()`.


### Input

The serializer expects input objects like this:

```javascript
{
    line1: 'foo',
    line2: {
        text: 'bar',    // max. 12 characters
        icon: Icon.INFO
    },
    line3: {
        block: 2,       // number of a predefined text block in WebUI
        icon: Icon.INFO
    },
    sound: {
        signal: Beeps.LONG_LONG,
        quantity: 1,    // 1 - 15, anything larger means 'continuously'
        distance: 30    // in seconds, use only multiples of 10 (10 - 160)
    },
    ledFlash: LedColor.GREEN
}
```

Any of the fields may be left out:

- `lineX` not specified → line remains unchanged
- `icon` not specified → no icon is shown
- `sound` not specified → no sound
- `ledFlash` not specified → no flashing

In order to clear a line, send a space to it.


### Output

The output of the serializer is the [config string](../spec).


## Development

You need Node.js and NPM, as always. Then, in this folder, run

    npm install

to install the development dependencies, and

    npm test

to run the test cases. This will also generate a coverage report in coverage\index.html.

We use VSCode as IDE, but the code does not require this. Just make sure your IDE's control files are not checked in.


## License

The code in this directory is provided under the terms of the [MIT license](LICENSE).
