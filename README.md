# hm-dis-ep-wm55

Specifications and code for accessing the Homematic display HM-Dis-EP-WM55

We have:

- Some [DIY specs](spec) which explain the structure of the config strings (under CC-BY-4.0 license)

  ![Railroad Diagram](spec/railroad-diagram.thumb.png)

- Some [JavaScript code](serializer) for creating such config strings from a JavaScript object (under MIT license)

- A [Node-RED flow](node-red) which makes the HM-Dis-EP-WM55 display available in Node-RED, based on the above (also under MIT license)

  ![Node-RED Flow](node-red/subflow.thumb.png)
