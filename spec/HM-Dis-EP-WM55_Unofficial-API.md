# HM-Dis-EP-WM55 Unofficial API

This document summarizes some of the details required to programmatically access the Homematic HM-Dis-EP-WM55 e-ink
display. The main thing is the railroad diagram describing the so-called "config string", see below.

**Firmware Version: 1.2** — If your display runs on a different firmware version, your mileage may vary!

Since EQ-3 support could not provide anything, I deduced this information from
[DrTob's script](https://homematic-forum.de/forum/viewtopic.php?t=43494#p433691),
a close look at the Homematic CCU3 WebUI, and a lot of trial &amp; error.


### Remarks

- The channels provided by the display device do not correctly expose their data points. I hope this will be fixed in
  a future firmware version. Until then, we may see some warnings in the log which we can ignore.
- Send the config string constructed via the railroad diagram to the following data point:  
  `BidCos-RF.NEQ1234567:3.SUBMIT`  
  where `NEQ1234567` is the serial number of your display device.
- The header and footer lines on the display cannot currently be changed using this technique. Use the WebUI for that,
  until we figure out a way. This is for the three middle lines at the moment.


## Diagram

Follow the railroad diagram in order to construct valid config strings.

For example, the following config string will print `Hello` and `World` on lines 1 and 2, and flash the LED in green:  
`0x02,0x0A,0x12,0x48,0x65,0x6c,0x6c,0x6f,0x0A,0x12,0x57,0x6f,0x72,0x6c,0x64,0x0A,0x0A,0x0A,0x14,0xC0,0x1C,0xD0,0x1D,null,0x16,0xF2,0x03`  
The entire thing is one string, no line breaks or other whitespace in it!


### Syntax

Loops which have a comment `2x` on them must be traveled exactly that many times. If the comment specifies a range,
such as `0 .. 11x`, then the loop must be traveled as many times as the range permits (between 0 and 11 times).


### Free Text

The free text characters are mostly ISO-8859-1 based, but several exceptions apply. Just try out your text. The
diagram already gives the most common characters and their representations. Free text can be 12 characters in length
per line, plus an optional icon.


### Audible Signal

The audible signal consists of three component settings:

- which beeps (long/short)
- how many times those beeps should be heard
- delay between audible signals

For example, the config string fragment `0x14,0xC5,0x1C,0xD2,0x1D,0xE0,0x16` would specify three long beeps, each
10 seconds apart.


### License

This document is provided for free under the terms of the
[CC-BY-4.0 license](https://gitlab.com/barfuin/hm-dis-ep-wm55/-/blob/master/LICENSE).\
Original Source: https://gitlab.com/barfuin/hm-dis-ep-wm55

<div style="page-break-after: always;"></div>

![Railroad Diagram](railroad-diagram.png)
