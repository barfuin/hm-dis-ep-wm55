# HM-Dis-EP-WM55 Unofficial API

In this folder, we collect information on how to access the Homematic HM-Dis-EP-WM55 e-ink display programmatically.

We currently have:

- A [DIY spec document](HM-Dis-EP-WM55_Unofficial-API.md)
- The [railroad diagram](railroad-diagram.png) contained in the document
- a [PDF version](HM-Dis-EP-WM55_Unofficial-API_v1.2.0.pdf) of the whole thing for sharing

The railroad diagram is created using the online
[railroad diagram generator](http://tabatkins.github.io/railroad-diagrams/) by Tab Atkins-Bittner.

Should you consider contributing an improvement to these documents, please update the .md and .txt files only.
The PNG and PDF will be created by a maintainer.
